
run

sudo apt-get install python3-venv
python3 -m venv auto24_venv
source auto24_venv/bin/activate
sudo apt-get install libopenjp2-7 libtiff5 libatlas-base-dev
pip install -r requirements.txt