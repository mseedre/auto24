import bs4
import requests
import csv
import time
from datetime import datetime
import sqlite3
from sqlite3 import Error
import sendEmail

from dotenv import load_dotenv
import os

import matplotlib.pyplot as plt
from dateutil import parser
from matplotlib import style
style.use('fivethirtyeight')

load_dotenv()

sender_email = os.getenv("SENDER_EMAIL")
receiver_email = os.getenv("RECEIVER_EMAIL")
# start_time = time.time()

# dummy comment for testing git Pull

cars = {
    "Octavia": "https://www.auto24.ee/kasutatud/nimekiri.php?bn=2&a=101&aj=&j=3&b=40&bw=177&ae=2&af=50&ag=0&ag=1&otsi=otsi",
    "Golf": "https://www.auto24.ee/kasutatud/nimekiri.php?bn=2&a=101&aj=&j=3&b=8&bw=2&ae=2&af=50&ag=0&ag=1&otsi=otsi",
    "X6": "https://www.auto24.ee/kasutatud/nimekiri.php?bn=2&a=100&b=4&bw=1161&ae=2&af=50&ag=1&otsi=otsi&ak=50",
    "lexus": "https://www.auto24.ee/kasutatud/nimekiri.php?bn=2&a=100&aj=&b=35&ae=2&af=50&ag=0&ag=1&otsi=otsi",
    "cayenne": "https://www.auto24.ee/kasutatud/nimekiri.php?bn=2&a=100&aj=&b=140&bw=236&ae=2&af=50&ag=0&ag=1&otsi=otsi",
    "HRV": "https://www.auto24.ee/kasutatud/nimekiri.php?bn=2&a=100&b=1&bw=164&f1=2017&ae=2&af=50&ssid=5481712"
    }


def get_page_contents(url_value):
    page = requests.get(url_value, headers={'User-Agent': 'Mozilla/5.0'})
    print("Web page fetched")
    return bs4.BeautifulSoup(page.text, "html.parser")


def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)


def get_data(conn):

    for car in cars:
        soup = get_page_contents(cars[car])
        #  print(car)
        qty = soup.find('div', class_='current-range')

        qty_value_list = qty.strong.contents
        qty_value = qty_value_list[0]

        today = datetime.now()

        today_formated = today.strftime('%Y-%m-%d %H:%M:%S')
        # print(today)
        row_content = (today_formated, car, qty_value)
        # print(row_content)
        insert_data(conn, row_content)

        # print("Number of {0}s in Auto24 currently on sale: {1}".format(car, qty_value[0]))

    # print("time elapsed: {:.2f}s".format(time.time() - start_time))


def create_connection(db_file):
    """ create a database connection to a SQLite database """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        # print(sqlite3.version)
    except Error as e:
        print(e)

    print("DB connection established")
    return conn


def checkIfTableexists(conn, table_name):

    cur = conn.cursor()
    cur.execute('''SELECT     name    FROM  sqlite_master    WHERE    type = 'table'    AND    name = "car_stat"''')

    rows = cur.fetchall()

    if rows:
        return True
    else:
        return False


def insert_data(conn, row_content):
    """
    Create a new project into the projects table
    :param conn:
    :param row_content:
    :return: project id
    """
    sql = "INSERT INTO car_stat(insert_date, car_name, count) VALUES{0}".format(row_content)
    cur = conn.cursor()
    # print(sql)
    cur.execute(sql)
    # print(cur.lastrowid)

    print("Data inserted to db")


def select_all_tasks(conn):
    """
    Query all rows in the tasks table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT * FROM car_stat")

    rows = cur.fetchall()
    emailBody = []

    for row in rows:
        emailBody.append(row)

    # pp = pprint.PrettyPrinter(indent=4)

    # pp.pprint(emailBody)
    # print(emailBody)
    return emailBody


def savetocsv(data):
    with open('output.csv', 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['rowid', 'date', 'model', 'qty'])
        writer.writerows(data)

    print("Data saved to csv file")


def main():
    database = r"auto24_stats.db"

    sql_create_stats_table = """ CREATE TABLE IF NOT EXISTS car_stat (
                                        id integer PRIMARY KEY AUTOINCREMENT,
                                        insert_date date,
                                        car_name text,
                                        count integer
                                    ); """

    # create a database connection
    conn = create_connection(database)

    # create tables
    if conn is not None:
        # create projects table
        create_table(conn, sql_create_stats_table)
        # get_data(conn)
        if checkIfTableexists(conn, 'car_stat'):
            get_data(conn)
            # select_all_tasks(conn)
            print("data received")
            q_result = select_all_tasks(conn)
            savetocsv(q_result)
            graph_data()

            sendEmail.sendEmail(sender_email, receiver_email, 'auto24 stats')

        else:
            create_table(conn, sql_create_stats_table)
            get_data(conn)

        conn.commit()
        conn.close()

    else:
        print("Error! cannot create the database connection.")


def graph_data():

    print("Started to draw graph")
    start_time = time.time()
    database = r"auto24_stats.db"
    conn = create_connection(database)
    cur = conn.cursor()
    cur.execute("""SELECT date(o.insert_date), o.count, c.count, l.count, x.count, g.count, h.count FROM car_stat o
                    left join car_stat c on date(c.insert_date)=date(o.insert_date) and c.car_name='cayenne'
                    left join car_stat l on date(l.insert_date)=date(o.insert_date) and l.car_name='lexus'
                    left join car_stat x on date(x.insert_date)=date(o.insert_date) and x.car_name='X6'
                    left join car_stat g on date(g.insert_date)=date(o.insert_date) and g.car_name='Golf'
                    left join car_stat h on date(h.insert_date)=date(o.insert_date) and h.car_name='HRV'
                    where o.car_name='Octavia'
                    """)
    data = cur.fetchall()

    dates = []
    octavia = []
    cayenne = []
    lexus = []
    x6 = []
    golf = []
    hrv = []

    for row in data:
        dates.append(parser.parse(row[0]))
        octavia.append(row[1])
        cayenne.append(row[2])
        lexus.append(row[3])
        x6.append(row[4])
        golf.append(row[5])
        hrv.append(row[6])

    plt.plot_date(dates, octavia, '-', linewidth=2, label="octavia")
    plt.plot_date(dates, cayenne, '-', linewidth=2, label="cayenne")
    plt.plot_date(dates, lexus, '-', linewidth=2, label="lexus")
    plt.plot_date(dates, x6, '-', linewidth=2, label="x6")
    plt.plot_date(dates, golf, '-', linewidth=2, label="golf")
    plt.plot_date(dates, hrv, '-', linewidth=2, label="HR-V")
    plt.legend(loc="upper left")
    plt.xticks(rotation=45)
    plt.xticks(fontsize=10)
    plt.subplots_adjust(bottom=0.164)
    # plt.show()
    plt.savefig('auto24_stat.png')

    print("Graph finished")
    print("time elapsed: {:.2f}s".format(time.time() - start_time))
    # print(data)
    # for row in data:
    #    print(row)


if __name__ == '__main__':
    main()
    # graph_data()
