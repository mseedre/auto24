import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from dotenv import load_dotenv
import os


load_dotenv()
port = 587  # For SSL
password = os.getenv("EMAIL_PASSWORD")


def sendEmail(fromaddr, toaddr, subject):

    # instance of MIMEMultipart
    msg = MIMEMultipart()

    # storing the senders email address
    msg['From'] = fromaddr

    # storing the receivers email address
    msg['To'] = toaddr

    # storing the subject
    msg['Subject'] = subject

    # string to store the body of the mail
    body = "Body_of_the_mail"

    # attach the body with the msg instance
    msg.attach(MIMEText(body, 'plain'))

    # open the file to be sent
    filename = "auto24_stat.png"
    attachment = open(r"auto24_stat.png", "rb")
    filename2 = "auto24_stats.db"
    attachment2 = open(r"auto24_stats.db", "rb")

    # instance of MIMEBase and named as p
    p = MIMEBase('application', 'octet-stream')
    p2 = MIMEBase('application', 'octet-stream')

    # To change the payload into encoded form
    p.set_payload(attachment.read())
    p2.set_payload(attachment2.read())

    # encode into base64
    encoders.encode_base64(p)
    encoders.encode_base64(p2)

    p.add_header('Content-Disposition', "attachment; filename= %s" % filename)
    p2.add_header('Content-Disposition', "attachment; filename= %s" % filename2)

    # attach the instance 'p' to instance 'msg'
    msg.attach(p)
    msg.attach(p2)

    # creates SMTP session
    s = smtplib.SMTP('smtp.gmail.com', port)

    # start TLS for security
    s.starttls()

    # Authentication
    s.login(fromaddr, password)

    # Converts the Multipart msg into a string
    text = msg.as_string()

    # sending the mail
    s.sendmail(fromaddr, toaddr, text)

    # terminating the session
    s.quit()
